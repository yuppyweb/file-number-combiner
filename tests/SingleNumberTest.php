<?php declare(strict_types=1);

namespace FileNumberCombiner\Test;

use FileNumberCombiner\FileNumberCombinerByDirectory;
use PHPUnit\Framework\TestCase;

class SingleNumberTest extends TestCase
{
    const TEST_FILES_DIRECTORY = __DIR__ . DIRECTORY_SEPARATOR . 'SingleNumberTestFiles';

    /**
     * @dataProvider dataProvider
     */
    public function testSumOfSingleNumbersFromFiles(array $fileNames, int|float $count): void
    {
        $fileNumberCombinerByDirectory = new FileNumberCombinerByDirectory(
            self::TEST_FILES_DIRECTORY
        );

        $this->assertEqualsWithDelta(
            $fileNumberCombinerByDirectory->getSumOfNumbersFromFiles(...$fileNames),
            $count,
            0.00000001
        );
    }

    public static function dataProvider(): array
    {
        return [
            [['count'], 209.2],
            [['count', 'total'], 149],
        ];
    }
}
