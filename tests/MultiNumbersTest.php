<?php declare(strict_types=1);

namespace FileNumberCombiner\Test;

use FileNumberCombiner\FileNumberCombinerByDirectory;
use PHPUnit\Framework\TestCase;

class MultiNumbersTest extends TestCase
{
    const TEST_FILES_DIRECTORY = __DIR__ . DIRECTORY_SEPARATOR . 'MultiNumbersTestFiles';

    /**
     * @dataProvider dataProvider
     */
    public function testSumOfMultiNumbersFromFiles(array $fileNames, int|float $count): void
    {
        $fileNumberCombinerByDirectory = new FileNumberCombinerByDirectory(
            self::TEST_FILES_DIRECTORY
        );

        $this->assertEqualsWithDelta(
            $fileNumberCombinerByDirectory->getSumOfNumbersFromFiles(...$fileNames),
            $count,
            0.00000001
        );
    }

    public static function dataProvider(): array
    {
        return [
            [['count'], 967.1],
            [['count', 'total'], -176.8],
        ];
    }
}
