<?php declare(strict_types=1);

namespace FileNumberCombiner;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use UnexpectedValueException;

readonly class FileNumberCombinerByDirectory
{
    private const REGEXP_OF_NUMBER_SEARCH = '/(-?\d+\.\d)|(-?\d+)/';

    public function __construct(private string $directory)
    {

    }

    /**
     * @throws UnexpectedValueException
     */
    public function getSumOfNumbersFromFiles(string ...$fileName): int|float
    {
        $sum = 0;
        $recursiveIterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->directory)
        );

        /** @var SplFileInfo $fileInfo */
        foreach ($recursiveIterator as $fileInfo) {
            if (
                $fileInfo->isFile() &&
                $fileInfo->isReadable() &&
                in_array($fileInfo->getFilename(), $fileName)
            ) {
                $sum += $this->getSumOfNumbersFromFileByPathName($fileInfo->getPathname());
            }
        }

        return $sum;
    }

    private function getSumOfNumbersFromFileByPathName(string $filePathName): int|float
    {
        $fileContent = file_get_contents($filePathName);
        preg_match_all(self::REGEXP_OF_NUMBER_SEARCH, $fileContent, $matches);

        return array_sum($matches[0]);
    }
}
