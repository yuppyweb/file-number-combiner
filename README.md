## How to add to a project

```
composer config repositories.yuppyweb git https://gitlab.com/yuppyweb/file-number-combiner.git
```
```
composer require yuppyweb/file-number-combiner:dev-main
```

## How to use

```
require_once 'vendor/autoload.php';

use FileNumberCombiner\FileNumberCombinerByDirectory;

$fileNumberCombinerByDirectory = new FileNumberCombinerByDirectory($directoryPath);
$sum = $fileNumberCombinerByDirectory->getSumOfNumbersFromFiles('count');

echo $sum;
```

## Run tests

```
composer test
```
